--------------------------------------------------------------------------------

                      GenFit Test for Dark Shine Simulation
                       --Generated from Geant4 example B2b

--------------------------------------------------------------------------------

[[_TOC_]]

# How to setup

in your fancy path:

```shell script
mkdir source build run
git clone git@gitlab.com:Biblehome/genfit-test-for-dark-shine.git source
cd build
source ../source/setup.sh
cmake -DCMAKE_INSTALL_PREFIX=../run ../source
make -j100
make install
```
# How to run

```shell script
cd ../run
example example.in
```
