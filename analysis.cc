#include <iostream>

#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"

int main(int argc, char** argv)
{
    TString input("dp_out.root");
    if(argc > 1)
        input = argv[1];

    if(gSystem->AccessPathName(input))
    {
        std::cout << "File not found: " << input << std::endl;
        return -1;
    }

    TFile *file = new TFile(input, "READ");
    TTree *tree = nullptr;
    file->GetObject("tree", tree);

    std::vector<float> *x = 0;
    std::vector<float> *y = 0;
    std::vector<float> *z = 0;
    tree->SetBranchAddress("x", &x);
    tree->SetBranchAddress("y", &y);
    tree->SetBranchAddress("z", &z);

    for(int i = 0; i < tree->GetEntries(); i++)
    {
        tree->GetEntry(i);

        if(z->size() == 5)
            std::cout << i << ": " << z->at(1) << std::endl;
    }

    return 0;
}
