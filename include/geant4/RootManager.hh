#ifndef ROOT_MANAGER_HH
#define ROOT_MANAGER_HH

//........................................//
//C++ stl
#include <iostream>
#include <vector>

//........................................//
//ROOT
#include "TFile.h"
#include "TTree.h"
#include "TString.h"

//........................................//
//Geant4
#include "G4Step.hh"
#include "G4Track.hh"
#include "G4VProcess.hh"
#include "G4SystemOfUnits.hh"

//........................................//
//GenFitTest
#include "geant4/TrackerHit.hh"

class RootManager
{
public:
//........................................//
//Constructor and destroyer
    RootManager();
    ~RootManager();

//........................................//
//Tree flow
    void Book();
    void Fill()  {tree->Fill(); }
    void Write() {tree->Write();}

//........................................//
//Set methods
    void SetEventNo(Int_t newEventNo) {eventNo = newEventNo;}
    void PosClear();
    void PosPushBack(Double_t newX, Double_t newY, Double_t newZ);
    //void SetX(Double_t newX)          {x = newX;            }
    //void SetY(Double_t newY)          {y = newY;            }
    //void SetZ(Double_t newZ)          {z = newZ;            }

private:
    G4String outfilename;
    TFile *file;
    TTree *tree;

    Int_t eventNo;
    std::vector<Double_t> x;
    std::vector<Double_t> y;
    std::vector<Double_t> z;
    //Double_t x;
    //Double_t y;
    //Double_t z;
};

#endif
