#!/bin/bash

echo "setting ROOT and Geant4 ..."
source /cvmfs/sft.cern.ch/lcg/views/LCG_97rc4python3/x86_64-centos7-gcc9-opt/setup.sh

export GENFIT_DIR="/home/zhuyifan/Software/GenFit/master/install"
echo "setting GenFit in ${GENFIT_DIR} ..."
export GENFIT_INCLUDE_DIR="${GENFIT_DIR}/include"
export GENFIT_LIB_DIR="${GENFIT_DIR}/lib64"
export LD_LIBRARY_PATH="${GENFIT_LIB_DIR}:$LD_LIBRARY_PATH"

export GEN_FOR_DS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
export GEN_FOR_DS_RUN_DIR="${GEN_FOR_DS_DIR}/../run"
echo "setting project in ${GEN_FOR_DS_RUN_DIR} ..."
if [ ! -d ${GEN_FOR_DS_RUN_DIR} ]; then
    echo "project run directory doesn't exsit, creating ..."
    mkdir ${GEN_FOR_DS_RUN_DIR} 
fi
export PATH="${GEN_FOR_DS_RUN_DIR}/:$PATH"
