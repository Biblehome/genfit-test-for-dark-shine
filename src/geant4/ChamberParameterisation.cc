//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file ChamberParameterisation.cc
/// \brief Implementation of the ChamberParameterisation class

#include "geant4/ChamberParameterisation.hh"

#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Box.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ChamberParameterisation::ChamberParameterisation(  
        G4int    countX,
        G4int    countY,
        G4int    countZ,
        G4double startZ,    //  Z of center of first 
        G4double chamberX,
        G4double chamberY,
        G4double spacingZ,  //  Z spacing of centers
        G4double cellThickness)
 : G4VPVParameterisation(),
   fCountX(countX),
   fCountY(countY),
   fCountZ(countZ),
   fStartZ(startZ),
   fChamberX(chamberX),
   fChamberY(chamberY),
   fSpacingZ(spacingZ),
   fCellThickness(cellThickness)
{
    fCellX = fChamberX/fCountX;
    fCellY = fChamberY/fCountY;
/*
    if(fCountX*fCountY*fCountZ > 0)
    {
        if(fSpacingZ < fHalfZ)
        {
            G4Exception("ChamberParameterisation::ChamberParameterisation()",
                        "InvalidSetup",
                        FatalException,
                        "Width>Spacing");
        }
    }
*/
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ChamberParameterisation::~ChamberParameterisation()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ChamberParameterisation::ComputeTransformation
(const G4int copyNo, G4VPhysicalVolume* physVol) const
{
    // Note: copyNo will start with zero!
    G4int idZ = copyNo/(fCountX*fCountY);
    G4int idY = (copyNo - idZ*fCountX*fCountY)/fCountX;
    G4int idX = copyNo - idZ*fCountX*fCountY - idY*fCountX;
    G4double xPosition = -0.5*fChamberX + (idX + 0.5)*fCellX;
    G4double yPosition = -0.5*fChamberY + (idY + 0.5)*fCellY;
    G4double zPosition = fStartZ + idZ*fSpacingZ;
    G4ThreeVector origin(xPosition, yPosition, zPosition);
    physVol->SetTranslation(origin);
    physVol->SetRotation(0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ChamberParameterisation::ComputeDimensions
(G4Box &trackerChamber, const G4int copyNo, const G4VPhysicalVolume*) const
{
    // Note: copyNo will start with zero!
    trackerChamber.SetXHalfLength(0.5*fCellX);
    trackerChamber.SetYHalfLength(0.5*fCellY);
    trackerChamber.SetZHalfLength(0.5*fCellThickness);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
