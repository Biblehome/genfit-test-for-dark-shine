#include "geant4/RootManager.hh"

//......................................../
//Constructor and destroyer
RootManager::RootManager() : outfilename("dp_out.root"),
                             file(nullptr),
                             tree(nullptr),
                             eventNo(0)
{
    x.clear();
    y.clear();
    z.clear();
}

RootManager::~RootManager()
{
    if(file) file->Close();
    delete file;
}

//......................................../
//Tree flow
void RootManager::Book()
{
    G4String fileName = outfilename;
    file = new TFile(fileName, "RECREATE");

    if (!file)
    {
        G4cout << " RootManager::book :"
               << " problem creating the ROOT TFile "
               << G4endl;
        return;
    }

    tree = new TTree("tree", "genfittest");
    tree->Branch("eventNo", &eventNo, "eventNo/I");
    tree->Branch("x", &x);
    tree->Branch("y", &y);
    tree->Branch("z", &z);
}

//........................................//
//Set methods
void RootManager::PosClear()
{
    x.clear();
    y.clear();
    z.clear();
}

void RootManager::PosPushBack(Double_t newX, Double_t newY, Double_t newZ)
{
    x.push_back(newX);
    y.push_back(newY);
    z.push_back(newZ);
}
